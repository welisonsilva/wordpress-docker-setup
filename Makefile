# Inicia o projeto
start:
	./start.sh

# Para o projeto
stop:
	./stop.sh

# Reinicia o projeot
restart:
	./restart.sh

# Apaga todos os arquivos do projeto que foram gerados
reset:
	./clear.sh

# Instala o Wordpress
installwp:
	./wpautoinstall.sh

# Exporta o Banco de Dados
exportbd:
	./export-db.sh

# Importa o Banco de Dados
importbd:
	./import-db.sh

# Configura o Slave
slavebd:
	./dbslave.sh
