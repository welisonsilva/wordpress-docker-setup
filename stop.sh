#!/usr/bin/env bash

source ./.env

# docker stop $(docker ps -a -q) && docker rm $(docker ps -aq) -f
echo "🛑 - Parando containers ${COMPOSE_PROJECT_NAME}"
docker stop \
    ${COMPOSE_PROJECT_NAME}_${PHPVERSION} \
    ${COMPOSE_PROJECT_NAME}_${DATABASE} \
    ${COMPOSE_PROJECT_NAME}_phpmyadmin \
    ${COMPOSE_PROJECT_NAME}_mailhog \
    ${COMPOSE_PROJECT_NAME}_${DATABASE}_slave \
    ${COMPOSE_PROJECT_NAME}_wpcli \
    ${COMPOSE_PROJECT_NAME}_phpredisadmin \
    ${COMPOSE_PROJECT_NAME}_redis
echo "⏹️ - Removendo ${COMPOSE_PROJECT_NAME}";
docker rm \
    ${COMPOSE_PROJECT_NAME}_${PHPVERSION} \
    ${COMPOSE_PROJECT_NAME}_${DATABASE} \
    ${COMPOSE_PROJECT_NAME}_phpmyadmin \
    ${COMPOSE_PROJECT_NAME}_mailhog \
    ${COMPOSE_PROJECT_NAME}_${DATABASE}_slave \
    ${COMPOSE_PROJECT_NAME}_wpcli \
    ${COMPOSE_PROJECT_NAME}_phpredisadmin \
    ${COMPOSE_PROJECT_NAME}_redis -f
# echo "- Todos os containers de mv localhost+2.pem /etc/apache2/ssl/cert.pem estão parados!!";