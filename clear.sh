#!/usr/bin/env bash

source ./.env


echo "Deseja mesmo DELETAR todos os dados do projeto $COMPOSE_PROJECT_NAME?"

select option in sim nao
do
        case $option in 
        sim|nao)   
                break
                ;;
        *)
            echo "Opção invalida" 
            ;;
        esac
done

if [ $option = "sim" ]; then
    echo "Parando projetos $COMPOSE_PROJECT_NAME no Docker?"
    ./stop.sh
    
    # Removing related folders/files...
    echo "Removendo arquivos relacionado ao projeto $COMPOSE_PROJECT_NAME?"
    sudo rm -rf ${MYSQL_DATA_DIR}
    sudo rm -rf ${APACHE_LOG_DIR}
    sudo rm -rf ${MYSQL_LOG_DIR}
    sudo rm -rf ${MYSQL_LOG_DIR_SLAVE}
    sudo rm -rf ${PHP_LOG_XDEBUG}
    sudo rm -rf ${CACHE_WPCLI}
    sudo rm -rf ${DOCUMENT_ROOT} 
    mkdir ${DOCUMENT_ROOT}

    echo "✔️ 🔥 Arquivos deletados com sucesso!";
else
    echo "✔️  Nenhum arquivo deletado";
fi