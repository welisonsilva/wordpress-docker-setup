#!/usr/bin/env bash

source ./.env

./start.sh
echo "✨ - Iniciando auto install Wordpress"
${DOCKER_COMPOSER_SYNTAX} -f docker-compose.yml -f wp-auto-config.yml run --rm wp-auto-config
./restart.sh