#!/bin/bash
# Local .env
source ./.env

read -p "O nome do banco de dados utilizado é '$MYSQL_DATABASE', gostaria de alterar?: " dbname
DATABASENAME=${dbname:-$MYSQL_DATABASE}
BD_DIR="./db"
# DATE=`date +%Y-%m-%d-%H%M%S` 
DATE=`date +%Y%m%d` 

echo 'O tipo de arquivo SQL ou GZIP?'
select typezip in sql sql.gz
do
        case $typezip in 
        sql|sql.gz)   
            break
            ;;
        *)
        echo "Opção inválida" 
        ;;
        esac
done

nameBD="${BD_DIR}/${DATABASENAME}_${DATE}.${typezip}"

if [ $typezip = "sql" ]; then
        # Arquivo SQL
        docker exec ${COMPOSE_PROJECT_NAME}_${DATABASE} /usr/bin/mysqldump -u $MYSQL_USER --password=$MYSQL_PASSWORD --add-drop-table $MYSQL_DATABASE > $nameBD
else
        # Arquivo SQL Zip
        docker exec ${COMPOSE_PROJECT_NAME}_${DATABASE} /usr/bin/mysqldump -u $MYSQL_USER --password=$MYSQL_PASSWORD --add-drop-table $MYSQL_DATABASE | gzip > $nameBD
fi
echo "Arquivo '${nameBD}' gerado com sucesso!"

