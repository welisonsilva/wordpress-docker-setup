
## Inspiração
- https://github.com/sprintcube/docker-compose-lamp
- https://www.datanovia.com/en/lessons/wordpress-docker-setup-files-example-for-local-development/
- https://www.datanovia.com/en/courses/install-wordpress-with-docker/

## Pastas e arquivos
- /.docker - Toda configuração para rodar o sistema.
- /db - pasta para importar e exportar o banco de dados
- /www - arquivos para rodar o aplicativo / sistema
- /.env - Configurações do Ambiente
- /tasks.TODO - Lista de tarefas
- /docker-composer.yml - Docker Composer


## Docker
- PHP (php54, php56, php71, php72, php73, php74, php8)
- Banco de dados Mysql e MariaDB
- PHPMyAdmin
- Wordpress
- WP CLI
- Redis - Cache
- Mail log - Email local
- Composer (php.7.4 ~ php.8)
- NVM para NodeJS (php.7.4 ~ php.8)
- Yarn (php.7.4 ~ php.8)
- NPM (php.7.4 ~ php.8)


## Etapas
1. Renomeie o arquivo sample.env para .env ```bash cp sample.env .env ```
2.  
- Instalar e configurar Banco de Dados Master e Slaver, rode o arquivos ./dbslave.sh ou make slavebd.

## Permissão nos arquivos shell *(.sh)*
```bash
# partindo da pasta principal
sudo chmod +x ./start.sh
sudo chmod +x ./stop.sh

sudo chmod +x ./wpautoinstall.sh
sudo chmod +x ./update-db.sh
sudo chmod +x ./dump-db.sh
sudo chmod +x ./clear.sh
sudo chmod +x ./dbslave.sh 
```

## Usando makefile
```bash
sudo apt install pv
sudo apt install make
```



## Rodar comandos diretamente no docker, sem entrar pelo /bash
```bash
# Exemplo WP CLI - Listar plugins instalados
docker exec -it ${NOME_DO_CONTAINER} bash -c "wp plugin list --allow-root"
```


## Problemas encontrados
### XDebug: não funciona o breakpoint no vscode no WSL2
- https://dev.to/getjv/xdebug-3-docker-laravel-vscode-52bi
- https://matthewsetter.com/setup-step-debugging-php-xdebug3-docker/

### SSL
- https://stackoverflow.com/questions/43752615/enable-apache-ssl-in-docker-for-local-development


## LINKS
- https://net-load.com/how-to-setup-mysql-database-master-and-slave-with-hyperdb-on-wordpress/
- http://www.devin.com.br/replicacao-mysql/ 