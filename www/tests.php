<?php

    if (isset($_GET['phpinfo'])) {
        phpinfo();
        exit;
    }

    if (isset($_GET['test_db'])) {
        $link = mysqli_connect('database', 'root', $_ENV['MYSQL_ROOT_PASSWORD'], null);

        if (!$link) {
            echo 'Error: Unable to connect to MySQL.'.PHP_EOL;
            echo 'Debugging errno: '.mysqli_connect_errno().PHP_EOL;
            echo 'Debugging error: '.mysqli_connect_error().PHP_EOL;
            exit;
        }

        echo 'Success: A proper connection to MySQL was made! The docker database is great.'.PHP_EOL;

        mysqli_close($link);
        exit;
    }

    if (isset($_GET['test_db_pdo'])) {
        $DBuser = 'root';
        $DBpass = $_ENV['MYSQL_ROOT_PASSWORD'];
        $pdo = null;

        try {
            $database = 'mysql:host=database:3306';
            $pdo = new PDO($database, $DBuser, $DBpass);
            echo 'Success: A proper connection to MySQL was made! The docker database is great.';
        } catch (PDOException $e) {
            echo "Error: Unable to connect to MySQL. Error:\n $e";
        }

        $pdo = null;
        exit;
    }

?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>LAMP STACK</title>
        
    </head>
    <body>
        <section class="hero is-medium is-info is-bold">
            <div class="hero-body">
                <div class="container has-text-centered">
                    <h1 class="title">
                        LAMP STACK
                    </h1>
                    <h2 class="subtitle">
                        Your local development environment
                    </h2>
                </div>
            </div>
        </section>
        <section class="section">
            <div class="container">
                <div class="columns">
                    <div class="column">
                        <h3 class="title is-3 has-text-centered">Environment</h3>
                        <hr>
                        <div class="content">
                            <ul>
                                <li><?php echo apache_get_version(); ?></li>
                                <li>PHP <?php echo phpversion(); ?></li>
                                <li>
                                    <?php
                                    $link = mysqli_connect('database', 'root', $_ENV['MYSQL_ROOT_PASSWORD'], null);

                                    /* check connection */
                                    if (mysqli_connect_errno()) {
                                        printf('MySQL connecttion failed: %s', mysqli_connect_error());
                                    } else {
                                        /* print server version */
                                        printf('MySQL Server %s', mysqli_get_server_info($link));
                                    }
                                    /* close connection */
                                    mysqli_close($link);
                                    ?>
                                </li>
                                <li>Mysql Slave</li>
                                <li>MailLog</li>
                                <li>Redis</li>
                                <li>Redis MyAdmin</li>
                            </ul>
                        </div>
                    </div>
                    <div class="column">
                        <h3 class="title is-3 has-text-centered">Quick Links</h3>
                        <hr>
                        <div class="content">
                            <ul>
                                <li><a href="/?phpinfo">phpinfo()</a></li>
                                <li><a href="http://localhost:<?php echo $_ENV['PMA_PORT']; ?>">phpMyAdmin</a></li>
                                <li><a href="/?test_db">Test DB Connection with mysqli</a></li>
                                <li><a href="/?test_db_pdo">Test DB Connection with PDO</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </body>
</html>
