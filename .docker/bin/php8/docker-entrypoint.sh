#!/bin/bash
set -euo pipefail

# Permissão
usermod --non-unique --uid 1000 www-data && groupmod --non-unique --gid 1000 www-data && chown -R www-data:www-data /var/www

# Install WP CLI para rodar em /var/www/html
curl -o /usr/local/bin/wp-cli.phar https://raw.githubusercontent.com/wp-cli/builds/gh-pages/phar/wp-cli.phar;
chmod +x /usr/local/bin/wp-cli.phar;
sudo -u www-data /usr/local/bin/wp-cli.phar
mv /usr/local/bin/wp-cli.phar /usr/local/bin/wp


# wp --info

# echo Fixing permissions...

# if [[ -v XDEBUG ]] && [ "$XDEBUG" = "true" ];
# then
# 	echo "Using XDEBUG";

#     inifile="/usr/local/etc/php/conf.d/pecl-xdebug.ini"
#     extfile="$(find /usr/local/lib/php/extensions/ -name xdebug.so)";
#     remote_port="${XDEBUG_IDEKEY:-9000}";
#     idekey="${XDEBUG_IDEKEY:-xdbg}";

#     if [ -f "$extfile" ] && [ ! -f "$inifile" ];
#     then
#         {
#             echo "[Xdebug]";
#             echo "zend_extension=${extfile}";
#             echo "xdebug.idekey=${idekey}";
#             echo "xdebug.remote_enable=1";
#             echo "xdebug.remote_connect_back=1";
#             echo "xdebug.remote_autostart=1";
#             echo "xdebug.remote_port=${remote_port}";
#         } > $inifile;
#     fi
#     unset extfile remote_port idekey;
# fi

exec "$@"
