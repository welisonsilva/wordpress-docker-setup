#!/usr/bin/env bash

source ./.env

if [ ${HOST_MACHINE_UNSECURE_HOST_PORT} = ${HOST_MACHINE_PMA_PORT} ]; then
    echo "🛑 - Error, você está usando a mesma porta de PHPMYADMIN";
else
    echo "✨ - Iniciando containers..."
    ${DOCKER_COMPOSER_SYNTAX} up ${DOCKER_COMPOSER_UP}
    
    if [ ${HOST_MACHINE_UNSECURE_HOST_PORT} = "80" ]; then
        echo "🆗 - Finalizado você já pode abrir ${WORDPRESS_WEBSITE_URL}";
    else
        echo "🆗 - Finalizado você já pode abrir ${WORDPRESS_WEBSITE_URL}:${HOST_MACHINE_UNSECURE_HOST_PORT}";
    fi
fi
set -e