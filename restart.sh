#!/usr/bin/env bash

source ./.env

# docker stop $(docker ps -a -q) && docker rm $(docker ps -aq) -f
echo "✨ - Reiniciando containers do projeto '${COMPOSE_PROJECT_NAME}'"
docker restart \
    ${COMPOSE_PROJECT_NAME}_${PHPVERSION} \
    ${COMPOSE_PROJECT_NAME}_${DATABASE} \
    ${COMPOSE_PROJECT_NAME}_phpmyadmin \
    ${COMPOSE_PROJECT_NAME}_mailhog \
    ${COMPOSE_PROJECT_NAME}_${DATABASE}_slave \
    ${COMPOSE_PROJECT_NAME}_wpcli \
    ${COMPOSE_PROJECT_NAME}_phpredisadmin \
    ${COMPOSE_PROJECT_NAME}_redis