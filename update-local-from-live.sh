#!/bin/bash
source ./.env


# EM TESTES, NÃO UTILIZAR A FUNÇÃO SED CONSUMINDO MUITA MEMORIA
# # Local .env

# echo "Generating a production DB dump"
# read -p "O nome do banco que será importado é $MYSQL_DATABASE, você quer mudar?: " dbname
# DATABASENAME=${dbname:-$MYSQL_DATABASE}
# # type="dev"

# echo 'Sufixo para DEV ou PROD?'

# select type in dev prod
# do
#         case $type in 
#         dev|prod)   
#                 break
#                 ;;
#         *)
#             echo "Opção inválida" 
#             ;;
#         esac
# done

# echo 'O tipo de arquivo SQL ou GZIP?'
# select typezip in sql sql.gz
# do
#         case $typezip in 
#         sql|sql.gz)   
#                 break
#                 ;;
#         *)
#             echo "Opção inválida"  
#             ;;
#         esac
# done

# SEARCH="https://wwwhml.universal.org"
# REPLACE="http://localhost"

# FILE="./bd/${DATABASENAME}_${type}.${typezip}"
# echo $FILE;

# if [ -f "$FILE" ]; then
#     echo "Importando arquivo $FILE"

#     if [ $typezip = "sql" ]; then
#         # Arquivo SQL
#         # 
#         pv $FILE | sed -i "s/$(printf '%s\n' "${SEARCH}" | sed -e 's/[\/&]/\\&/g')/$(printf '%s\n' "${REPLACE}" | sed -e 's/[\/&]/\\&/g')/g" $FILE | docker exec -i ${COMPOSE_PROJECT_NAME}_${DATABASE} mysql -u $MYSQL_USER --password=$MYSQL_PASSWORD $MYSQL_DATABASE < $FILE
#     else
#         # Arquivo SQL Zip
#         pv $FILE | gunzip | docker exec -i ${COMPOSE_PROJECT_NAME}_${DATABASE} mysql -u $MYSQL_USER --password=$MYSQL_PASSWORD $MYSQL_DATABASE
#     fi

# else 
#     echo "$FILE não existe."
# fi

# sudo apt-get install pv

read -p "O nome do banco que será importado é $MYSQL_DATABASE, você quer mudar?: " dbname
DATABASENAME=${dbname:-$MYSQL_DATABASE}
BD_DIR="./db"

dbsearch_default='localhost'
dbreplace_default='localhost199'

read -p "Procurar por? exemplo: ${dbsearch_default} " dbsearch
read -p "Procurar por? exemplo: ${dbreplace_default}: " dbreplace
SEARCH=${dbsearch:-${dbsearch_default}}
REPLACE=${dbreplace:-${dbreplace_default}}


echo 'O tipo de arquivo SQL ou GZIP?'
select typezip in sql sql.gz
do
        case $typezip in 
        sql|sql.gz)   
                break
                ;;
        *)
            echo "Opção inválida"  
            ;;
        esac
done

FILE="${BD_DIR}/${DATABASENAME}.${typezip}"
pv $FILE | sed -i "s/$(printf '%s\n' "${SEARCH}" | sed -e 's/[\/&]/\\&/g')/$(printf '%s\n' "${REPLACE}" | sed -e 's/[\/&]/\\&/g')/g" $FILE
echo "Finalizado, foi trocado ${SEARCH} por ${REPLACE}, no arquivo ${FILE}!"